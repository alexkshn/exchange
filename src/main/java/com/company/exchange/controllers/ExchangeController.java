package com.company.exchange.controllers;

import com.company.exchange.model.ExchangeRate;
import com.company.exchange.model.dto.BalanceDto;
import com.company.exchange.model.dto.ExchangeDto;
import com.company.exchange.model.dto.ExchangeRateDto;
import com.company.exchange.model.enums.CurrencyCode;
import com.company.exchange.services.ExchangeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/exchange")
@RequiredArgsConstructor
public class ExchangeController {

    private final ExchangeService exchangeService;

    @PostMapping("/rate")
    public ResponseEntity<String> insertExchangeRate(@RequestBody ExchangeRateDto rate) {
        exchangeService.saveExchangeRate(rate);
        return new ResponseEntity<>("ExchangeRate inserted successfully!", HttpStatus.OK);
    }

    @GetMapping("/rate")
    public ResponseEntity<Optional> getExchangeRate(@RequestParam("currency") CurrencyCode code) {
        Optional<ExchangeRate> rate = exchangeService.getCurrentExchangeRateForCurrency(code);
        return new ResponseEntity<>(rate, HttpStatus.OK);
    }

    @PostMapping("/")
    ResponseEntity<String> insertExchange(@RequestBody ExchangeRateDto exchange) {
        exchangeService.saveExchange(exchange);
        return new ResponseEntity<>("Exchange performed successfully", HttpStatus.OK);
    }

    @PutMapping("/balance")
    ResponseEntity<String> updateBalance(@RequestBody BalanceDto balanceDto) {
        exchangeService.updateBalance(balanceDto);
        return new ResponseEntity<>("Balance updated successfully", HttpStatus.OK);
    }
}
