package com.company.exchange.repositories;

import com.company.exchange.model.ExchangeRate;
import com.company.exchange.model.enums.CurrencyCode;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface ExchangeRateRepository extends CrudRepository<ExchangeRate, Long> {

    Optional<ExchangeRate> findFirstByCreatedOn(LocalDate date);

    Optional<ExchangeRate> findByCurrencyCodeAndCreatedOn(CurrencyCode code, LocalDate date);
}
