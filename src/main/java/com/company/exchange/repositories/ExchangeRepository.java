package com.company.exchange.repositories;

import com.company.exchange.model.Exchange;
import org.springframework.data.repository.CrudRepository;

public interface ExchangeRepository extends CrudRepository<Exchange, Long> {
}
