package com.company.exchange.repositories;

import com.company.exchange.model.Balance;
import com.company.exchange.model.enums.CurrencyCode;
import org.springframework.data.repository.CrudRepository;

public interface BalanceRepository extends CrudRepository<Balance, Long> {

    Balance findFirstByCurrencyCode(CurrencyCode currencyCode);
}
