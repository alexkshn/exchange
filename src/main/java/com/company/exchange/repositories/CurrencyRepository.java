package com.company.exchange.repositories;

import com.company.exchange.model.Currency;
import com.company.exchange.model.enums.CurrencyCode;
import org.springframework.data.repository.CrudRepository;

public interface CurrencyRepository extends CrudRepository<Currency, Long> {

    Currency findFirstByCode(CurrencyCode code);
}
