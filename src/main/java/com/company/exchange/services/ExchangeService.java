package com.company.exchange.services;

import com.company.exchange.model.Balance;
import com.company.exchange.model.Currency;
import com.company.exchange.model.Exchange;
import com.company.exchange.model.ExchangeRate;
import com.company.exchange.model.dto.BalanceDto;
import com.company.exchange.model.dto.ExchangeDto;
import com.company.exchange.model.dto.ExchangeRateDto;
import com.company.exchange.model.enums.CurrencyCode;
import com.company.exchange.repositories.BalanceRepository;
import com.company.exchange.repositories.CurrencyRepository;
import com.company.exchange.repositories.ExchangeRateRepository;
import com.company.exchange.repositories.ExchangeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ExchangeService {

    private final ExchangeRateRepository exchangeRateRepository;
    private final CurrencyRepository currencyRepository;
    private final ExchangeRepository exchangeRepository;
    private final BalanceRepository balanceRepository;

    public void saveExchangeRate(ExchangeRateDto exchangeRateDto) {

        Currency currency = currencyRepository.findFirstByCode(exchangeRateDto.getCurrency());

        ExchangeRate rate = ExchangeRate.builder()
                .amount(exchangeRateDto.getAmount())
                .currency(currency)
                .rate(exchangeRateDto.getRate())
                .createdOn(LocalDate.now())
                .build();

        Optional<ExchangeRate> currentExchangeRate = getCurrentExchangeRateForCurrency(exchangeRateDto.getCurrency());
        currentExchangeRate.ifPresent(exchangeRate -> rate.setId(exchangeRate.getId()));
        exchangeRateRepository.save(rate);
    }

    public Optional<ExchangeRate> getCurrentExchangeRateForCurrency(CurrencyCode code) {
        LocalDate currentDate = LocalDate.now();
        return exchangeRateRepository.findByCurrencyCodeAndCreatedOn(code, currentDate);
    }

    public void saveExchange(ExchangeRateDto exchangeDto) {
        Currency currency = currencyRepository.findFirstByCode(exchangeDto.getCurrency());

        Exchange exchange = Exchange.builder()
                .currency(currency)
                .rate(exchangeDto.getRate())
                .amount(exchangeDto.getAmount())
                .build();
        exchangeRepository.save(exchange);
    }

    public void updateBalance(BalanceDto balanceDto) {
        Balance balance = balanceRepository.findFirstByCurrencyCode(balanceDto.getCurrency());
        balance.setAmount(balanceDto.getAmount());
        balanceRepository.save(balance);
    }
}
