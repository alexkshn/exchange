package com.company.exchange.model.enums;

public enum CurrencyCode {
    EUR,
    KZT,
    USD,
    JPY
}
