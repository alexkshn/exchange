package com.company.exchange.model.dto;

import com.company.exchange.model.enums.CurrencyCode;
import lombok.Data;

@Data
public class BalanceDto {

    private CurrencyCode currency;
    private Double amount;
}
