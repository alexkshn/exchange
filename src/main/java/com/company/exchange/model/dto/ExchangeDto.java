package com.company.exchange.model.dto;

import com.company.exchange.model.enums.CurrencyCode;
import lombok.Data;

@Data
public class ExchangeDto {

    private CurrencyCode currency;
    private Double rate;
    private Double amount;
}
