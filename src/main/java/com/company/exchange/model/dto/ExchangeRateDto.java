package com.company.exchange.model.dto;

import com.company.exchange.model.enums.CurrencyCode;
import lombok.Data;

@Data
public class ExchangeRateDto {

    private Double amount;
    private Double rate;
    private CurrencyCode currency;
}
