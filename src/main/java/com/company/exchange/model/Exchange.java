package com.company.exchange.model;


import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "exchange")
@Entity
@Data
@Builder
public class Exchange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "rate")
    private Double rate;

    @Column(name = "exchange_time", insertable = false, updatable = false)
    private LocalDateTime exchangeTime;

    @OneToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    private Currency currency;

}
