-- Currencies
INSERT INTO "currencies"
VALUES (1, 'EUR');

INSERT INTO "currencies" ("id", "currency")
VALUES (2, 'USD');

INSERT INTO "currencies" ("id", "currency")
VALUES (3, 'KZT');

INSERT INTO "currencies" ("id", "currency")
VALUES (4, 'JPY');

--Exchange rates

INSERT INTO exchange_rates ("id", "currency_id", "amount", "rate")
VALUES ('1', '1', '10', '19.70');

INSERT INTO exchange_rates ("id", "currency_id", "amount", "rate")
VALUES ('2', '2', '10', '16.75');

INSERT INTO exchange_rates ("id", "currency_id", "amount", "rate")
VALUES ('3', '3', '10', '0.40');

INSERT INTO exchange_rates ("id", "currency_id", "amount", "rate")
VALUES ('4', '4', '100', '16.00');

--Initial balances

INSERT INTO "balance" ("id", "amount", "currency_id")
VALUES ('1', '0', '1');

INSERT INTO "balance" ("id", "amount", "currency_id")
VALUES ('2', '0', '2');

INSERT INTO "balance" ("id", "amount", "currency_id")
VALUES ('3', '0', '3');

INSERT INTO "balance" ("id", "amount", "currency_id")
VALUES ('4', '0', '4');


