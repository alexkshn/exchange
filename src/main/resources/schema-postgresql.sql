CREATE TABLE currencies (
	id serial PRIMARY KEY,
	currency  VARCHAR (255)
);

CREATE TABLE exchange_rates (
    id             SERIAL PRIMARY KEY,
	currency_id    INTEGER,
    amount         FLOAT(10),
    rate           FLOAT(10),
	created_on     TIMESTAMP DEFAULT CURRENT_DATE,
    FOREIGN KEY (currency_id)
         REFERENCES currencies (id)
);

CREATE TABLE balance (
   id          SERIAL PRIMARY KEY ,
   amount      FLOAT(10),
   currency_id INTEGER,
   FOREIGN KEY (currency_id)
         REFERENCES currencies (id)
);

CREATE TABLE exchange(
    id   SERIAL      PRIMARY KEY,
    currency_id      INTEGER ,
    amount           FLOAT(10),
    rate             FLOAT(10),
    exchange_time    TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);