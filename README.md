# Exchange app

## Exchange app run:
Required installations: 
Java 8 , Maven, PostgresDB or Docker.

If you have docker run: 
docker-compose up from /docker directory to install PostgresDB

Database creation scripts are located in src/main/resources/ folder: 
schema.sql and data.sql

Database scripts are triggered on application start for the first time.
Remove property to stop triggering the scripts: 

``` 
spring.datasource.initialization-mode=always
```


1. Run terminal command to start the app:

```
mvn springboot:run
```

Endpoints usage: 

``` 
1. Publish new exchange rate for currency.
   
   POST http://localhost:8081/exchange/rate
 
   {
 	  "amount": 10, -- Amount of currency 
 	  "rate" : 11.0, -- Exchange rate
 	  "currency" : "EUR" - Currency code
   } 

2. Get current exchange rate for given currency. 

   GET http://localhost:8081/exchange/rate?currency=USD -- Currency code for exchange rate
  

 
3. Perform exchange operation

   POST http://localhost:8081/exchange/
    
     {
     	"currency" : "EUR", -- Currency code
     	"rate" : "19.80",   -- Exchange rate
     	"amount" : "100"    -- Amount of money
     }

4. Perform balance update .

   PUT http://localhost:8081/exchange/balance

     {
       "currency":"EUR", -- Currency code
       "amount" : 100   -- Amount
     }

```